import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MrVietRoutingModule } from './mr-viet-routing.module';
import { MyTestComponent } from './my-test/my-test.component';


@NgModule({
  declarations: [
    MyTestComponent
  ],
  imports: [
    CommonModule,
    MrVietRoutingModule
  ]
})
export class MrVietModule { }
